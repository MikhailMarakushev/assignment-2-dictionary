asm = nasm
asmFlag = -f elf64

program: main.o dict.o lib.o
	ld -o $@ $^

main.o: main.asm colon.inc lib.inc
	$(asm) $(asmFlag)  $<

%.o :%.asm
	$(asm) $(asmFlag) $^
	
clean:
	rm *.o program

.PHONY: clean
