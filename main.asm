%include "words.inc"
%include "lib.inc"
%define MaxLen 256
%define OFFSET 8
%define WRITE 1
%define ERROR 2
%define WRONGSIZE 3
%define NOVALUE 4


section .bss
buffer: times MaxLen db 0

section .rodata
error: db "Try typing a shoter one", 0
noValue: db "Sorry, but there is no element with this name!", 0

section .text

extern exit
extern find_word

global _start

print_error:
        push rdi
        call string_length
        mov rdx, rax
        mov rax, WRITE
        pop rsi
        mov rdi, ERROR
        syscall 
        call print_newline
        jmp exit

_start:
        mov rdi, buffer
        mov rsi, MaxLen
        call read_word
        test rax, rax 
        je .OV
        mov rdi, buffer
        mov rsi, pointer
        push rsi
        call find_word
        pop rsi
        cmp rax, 0 
        je .noValue
        add rax, OFFSET
        mov rdi, rax
        push rdi
        call string_length
        pop rdi
        add rdi, rax
        inc rdi
        call print_string
        call print_newline
        jmp exit
        .OV:
                mov rdi, error
                call print_error
                mov rdi, WRONGSIZE

        .noValue:
                mov rdi, noValue
                call print_error
                mov rdi, NOVALUE

          
