%define pointer 0
%macro colon 2 			
	%ifid %2
		%2: dq pointer
		%define pointer %2
	%else
		%error "You should put an identifier to current element in the second element"
	%endif
	%ifstr %1
		db %1, 0 ; Задаем ключ
	%else
		%error "That's not a string!"
	%endif
%endmacro