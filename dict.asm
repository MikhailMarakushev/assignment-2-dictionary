%include "lib.inc"
%define OFFSET 8
global find_word

section .text

find_word:
	push r13
	push r12
.loop:				
	test rsi, rsi
	je	.stop	
	add	rsi, OFFSET
	call string_equals
	mov     r13, rsi
    mov     r12, rdi
	test rax, rax
	jne .nice
	mov	rsi, [rsi]
	jmp	.loop	
.nice:	
	mov	rax, rsi
	ret	
.stop:
	xor	rax, rax
	ret	